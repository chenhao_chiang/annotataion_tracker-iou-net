'''
Author: Chen-Hao Chiang
Created in December 2018
'''
import cv2
import argparse
import glob
import numpy as np
import os
import time
import sys
import tensorflow as tf
import threading
import pickle
import json

BATCH_SIZE = 1

basedir = os.path.dirname(__file__)
sys.path.append(os.path.abspath(os.path.join(basedir, os.path.pardir)))
from tracker import re3_tracker

from TrackerIoUNet import TrackerIoUNet

from re3_utils.util import drawing
from re3_utils.util import bb_util
from re3_utils.util import im_util
from re3_utils.util import IOU
from model import generalObject as GeneralObject

from constants import OUTPUT_WIDTH
from constants import OUTPUT_HEIGHT
from constants import PADDING

np.set_printoptions(precision=6)
np.set_printoptions(suppress=True)

imageSource = ""
videoName = ""
pickleFileName = ""
pickleFilePath = ""
totalNumberOfFrames = 0
numberOfDigits = 8

INITIAL = True

# Timer mode
TIMER = False
TIMER_OBJECT_COUNTER = 0

# Annotation modes
ANNOTATION_MODE = 3
MANUAL_MODE = 1
MANUAL_MODE_INTERVAL = 5
TRACKER_MODE = 2
IOU_MODE = 3
IOU_STOP_MODE = 4
IOU_MODE_INTERVAL = 100

# Main program
INITIAL_FRAME = 1
frameNum = INITIAL_FRAME
stopWaitingForUserInput = False

# Tracker-IoU-Net related 
trackerIoUNet = None
sessIOU = tf.Session()

# Trackbar related
trackbarPos = INITIAL_FRAME
stopFrameTrackbar = False
setTrackbarByFunction = False
setByFineTrackbar = False
setByRoughTrackbar = False
prevFineTrackbarPos = 50

# Hyperparameters
COMPARE_FRAME_OFFSET = 10
IOU_THRESHOLD = 0.6

# multi tracks
idCounter = 0
allObjects = []
trackedObjects = [] # array of GeneralObjects
allClasses = [] # array of String
objectsToMerge = [] # objects to merge

def getNewKey(key):
    return key + "a"

def getOldKey(key):
    return key.replace("a","")

def trackBackward(objectList,frame,numberOfFrames,stopByIOU=True):
    global tracker, IOU_THRESHOLD, INITIAL

    currentFrame = frame

    # do not track backward for the initial frame
    if INITIAL:
        return

    num = numberOfFrames
    if num >= frame:
        num = frame - 1

    imgFile = getFileNameFromFrame(imageSource,currentFrame,numberOfDigits)
    img = cv2.imread(imgFile)

    newObjects = objectList
    newObjectsId = [] # onjects with transformed ids
     
    objDict = {}

    for obj in newObjects:
        obj.setTrackedBackward(True)
        newKey = getNewKey(obj.getId())
        addToList(newObjectsId,newKey)
        objDict[newKey] = obj.getBox(str(currentFrame))
    
    try:
        for i in range(numberOfFrames):
            if len(newObjectsId) == 0:
                break
            else: 
                # more than one objects
                bboxes = []
                if i == 0:
                    bboxes = getBboxesFromTracker(img,objDict,newObjectsId)
                    objDict = {}
                else:
                    imgFile = getFileNameFromFrame(imageSource,currentFrame - i,numberOfDigits)
                    img = cv2.imread(imgFile)
                    bboxes = getBboxesFromTracker(img,{},newObjectsId)
                for index, box in enumerate(bboxes):
                    oid = getOldKey(newObjectsId[index])
                    f = currentFrame - i
                    modifyBox(oid,f,box)
            
            if stopByIOU:
                if i == 0:
                    continue

                (obj_batch, ious) = calculateIOU(currentFrame - i,newObjects,True)
                
                if len(ious) == 0:
                    continue

                for index, o in enumerate(obj_batch):
                    if float(ious[index][0]) < IOU_THRESHOLD:
                        removeFromList(newObjects,o)
                        removeFromList(newObjectsId,getNewKey(o.getId()))
                        o.removeBox(str(currentFrame - i))
                        o.setTrackedBackward(False)
                    else: # check collision
                        collidedObj = checkCollision(o,currentFrame-i)
                        if collidedObj:
                            addToList(objectsToMerge,[collidedObj,o,currentFrame - i])
                            removeFromList(newObjects,o)
                            removeFromList(newObjectsId,getNewKey(o.getId()))
                            o.setTrackedBackward(False)
        for obj in newObjects:
            obj.setTrackedBackward(False)
    except:
        saveDict(allObjects,pickleFilePath)
        raise

def checkCollision(obj,frame):
    global allObjects

    for o in allObjects:
        if o.getId() == obj.getId():
            continue
        if o.getBox(str(frame)) is None or obj.getBox(str(frame)) is None:
            continue
        if IOU.IOU(o.getBox(str(frame)),obj.getBox(str(frame))) > 0.5 and o.getObjectClass() == obj.getObjectClass():
            return o
    
    return None

# merge obj into targetObj
def mergeObjects(obj,targetObj):
    global allObjects, trackedObjects

    # return True if one of the objects has already been removed
    if obj not in allObjects or targetObj not in allObjects: 
        return True

    # return False if one of the objects is being tracked backward 
    # -> wait until object is not being tracked backward
    if obj.getTrackedBackward() or targetObj.getTrackedBackward():
        return False

    i = 1

    while True:

        if i > totalNumberOfFrames:
            break

        key = str(i)
        i += 1

        # copy box
        if obj.getBox(key) is not None:
            targetObj.addBox(key,obj.getBox(key))

        # copy retrackFrame
        if obj.retrackForFrame(key) is not None:
            targetObj.addRetrackFrame(key,obj.retrackForFrame(key))
        else:
            targetObj.addRetrackFrame(key,None)

        # copy IOU
        if obj.getIOU(key) is not None:
            targetObj.setIOU(key,obj.getIOU(key))
        
    removeFromList(allObjects,obj)
    removeFromList(trackedObjects,obj)
    
    return True


def startAnnotation():
    global ANNOTATION_MODE, TIMER, TIMER_OBJECT_COUNTER, INITIAL, INITIAL_FRAME, frameNum, stopFrameTrackbar, trackbarPos, tracker, allObjects, trackedObjects
    cv2.namedWindow('Webcam', cv2.WINDOW_NORMAL)
    cv2.resizeWindow('Webcam', OUTPUT_WIDTH, OUTPUT_HEIGHT)

    timerFile = createTimerFile() # used for recording computation time in the timer mode
    frameDidDecrement = False # this variable indicates if the frameNum was decremented due to annotations

    try:
        startTime = time.time()
        while True:
            # trackbar interruption         
            if stopFrameTrackbar:
                frameNum = trackbarPos
                if INITIAL:
                    stopFrameTrackbar = False
            else: # update trackbar position
                setTrackbarToFrame("test", "Webcam", frameNum)

            imgFile = getFileNameFromFrame(imageSource,frameNum,numberOfDigits)
            img = cv2.imread(imgFile)

            # End of video
            if img is None:
                break

            origImg = img.copy()
            drawImg = img.copy()

            if TIMER and INITIAL:
                modifyBox("n",frameNum,[388,141,460,213])

            if INITIAL:
                showRectForAllObjects('Webcam',drawImg,frameNum,annotate=True)
                showAssistanceWindow('Assistance',frameNum)

                if not TIMER:
                    waitForUserInput(drawImg)
                
                # if trackbar value changed -> change frameNum
                if stopFrameTrackbar:
                    frameNum = trackbarPos
                    drawImg = origImg

            
            if TIMER:
                if TIMER_OBJECT_COUNTER == 10:
                    break
                if frameNum == INITIAL_FRAME:
                    startTime = time.time()
                if frameNum == INITIAL_FRAME + 100:
                    deleteAllObjectTracks()
                    frameNum = INITIAL_FRAME
                    TIMER_OBJECT_COUNTER += 1 
                    now = time.time()
                    if ANNOTATION_MODE == TRACKER_MODE:
                        timerFile = open('./timerTracker.txt', 'a')
                    if ANNOTATION_MODE == IOU_STOP_MODE:
                        timerFile = open('./timerTrackerIOU.txt', 'a')
                    timerFile.write("%s\n" % (now - startTime))
                    timerFile.close()
                    print("--- %s seconds ---" % (now - startTime))
                    continue
            
            linesToDelete = []
            for line in objectsToMerge:
                if mergeObjects(line[0],line[1]):
                    addToList(linesToDelete,line)

            for line in linesToDelete:
                removeFromList(objectsToMerge,line)

            # Decide which objects to track
            newObjects, objectsToTrackBackward = decideWhichObjectsToTrack(frameNum)

            if not TIMER and (ANNOTATION_MODE == IOU_MODE or ANNOTATION_MODE == IOU_STOP_MODE) and len(objectsToTrackBackward) > 0:
                trackBackward_thread = threading.Thread(target=trackBackward, args=[objectsToTrackBackward,frameNum,IOU_MODE_INTERVAL,True])
                trackBackward_thread.start()

            newObjectsDict = dict()

            for o in newObjects:
                newObjectsDict[o.getId()] = o.getBox(str(frameNum))

            trackedObjectsId = list(map(lambda o: o.getId(), trackedObjects))

            # get output bboxes from the tracker
            bboxes = []
            if ANNOTATION_MODE != MANUAL_MODE:
                bboxes = getBboxesFromTracker(img, newObjectsDict,trackedObjectsId)
            
            # save the bboxes in the dict    
            for index, bbox in enumerate(bboxes):            
                o = trackedObjects[index]
                modifyBox(o.getId(),frameNum,bbox)
                
            # handle collision
            for o in allObjects:
                if checkCollision(o,frameNum):
                    mergeObjects(checkCollision(o,frameNum),o)

            # calculate ious
            (stop_frame_iou, batchObjs, ious) = (False, [], [])
            if ANNOTATION_MODE == IOU_MODE or ANNOTATION_MODE == IOU_STOP_MODE:
                (batchObjs, ious) = calculateIOU(frameNum,allObjects)
            
            # remove objects with low iou
            if not TIMER:
                removeObjectsWithLowIou(frameNum,batchObjs)

            # decide if the program should pause
            stop_frame = False
            stop_frame_mode = False

            if ANNOTATION_MODE == MANUAL_MODE:
                stop_frame_mode = True

            if ANNOTATION_MODE == IOU_STOP_MODE and frameNum % IOU_MODE_INTERVAL == 0:
                stop_frame_mode = True

            if not INITIAL:
                if stop_frame_mode or stopFrameTrackbar:
                    stop_frame = True
            else:
                INITIAL = False

            # shows bboxes if in tracker mode
            if ANNOTATION_MODE != MANUAL_MODE:
                showRectForAllObjects('Webcam',drawImg,frameNum,annotate=stop_frame)

            showAssistanceWindow("Assistance",frameNum) 

            if stop_frame and not TIMER:
                if ANNOTATION_MODE == MANUAL_MODE:
                    showRectForAllObjects('Webcam',drawImg,frameNum,annotate=stop_frame)
                
                stop_frame = False
                
                if stopFrameTrackbar:
                    stopFrameTrackbar = False
                    if ANNOTATION_MODE == MANUAL_MODE:
                        waitForUserInput(origImg,shouldDecrementFrameNumAfterAnnotation=False)
                    else:
                        frameDidDecrement = waitForUserInput(origImg,shouldDecrementFrameNumAfterAnnotation=True)
                elif frameDidDecrement:
                    frameDidDecrement = False
                else:
                    if ANNOTATION_MODE == MANUAL_MODE:
                        frameDidDecrement = waitForUserInput(origImg,shouldDecrementFrameNumAfterAnnotation=False)
                    else:
                        frameDidDecrement = waitForUserInput(origImg,shouldDecrementFrameNumAfterAnnotation=True)
            
            keyPressed = cv2.waitKey(1)

            if keyPressed == ord(' '):
                if ANNOTATION_MODE != MANUAL_MODE:
                    waitForUserInput(origImg,shouldDecrementFrameNumAfterAnnotation=True)
            # elif keyPressed == ord('r'):
            #     if ANNOTATION_MODE == TRACKER_MODE:
            #         userRemoveObject()
            elif keyPressed == ord('e'):
                saveDict(allObjects,pickleFilePath) 
                break
            
            if ANNOTATION_MODE == MANUAL_MODE:
                frameNum += MANUAL_MODE_INTERVAL
            else:
                frameNum += 1

    except:
        saveDict(allObjects,pickleFilePath)   
        raise 

    cv2.destroyAllWindows()

def decideWhichObjectsToTrack(frame):
    global allObjects, trackedObjects, stopFrameTrackbar
    newObjects = []
    objectsToTrackBackward = []
    
    key = str(frame)
    prevKey = str(frame - 1)
    nextKey = str(frame + 1)

    for obj in allObjects:
        if obj.getBox(key) is None: # current box absent
            if stopFrameTrackbar:
                removeFromList(trackedObjects,obj)
            continue
        if obj.retrackForFrame(key): # re-track 
                obj.removeTrackFromFrame(frame+1)
                addToList(newObjects,obj)
                obj.addRetrackFrame(key,None)
        if obj.getBox(prevKey) is not None: # previous box present
            if obj.retrackForFrame(key) is None: # re-track unspecified
                if obj.getBox(nextKey) is not None: # next box present
                    removeFromList(trackedObjects,obj)
                else: # next box absent
                    addToList(newObjects,obj)
            elif obj.retrackForFrame(key) is False: # re-track false
                removeFromList(trackedObjects,obj)
        else: # previous box absent
            if obj.retrackForFrame(key) is None: # re-track not specified
                if obj.getBox(nextKey) is None: 
                    addToList(newObjects,obj)
                    addToList(objectsToTrackBackward,obj)

    for o in newObjects:
        addToList(trackedObjects,o)

    return newObjects, objectsToTrackBackward

def createTimerFile():
    if TIMER and ANNOTATION_MODE == TRACKER_MODE:
        timerFile = open('./timerTracker.txt', 'w')
        timerFile.close()
        return timerFile
    if TIMER and ANNOTATION_MODE == IOU_STOP_MODE:
        timerFile = open('./timerTrackerIOU.txt', 'w')
        timerFile.close()
        return timerFile
    return None

def getFileNameFromFrame(source,frame,numberOfDigits):
    return source + str(frame).zfill(8) + '.jpg'

def setTrackbarToFrame(trackbarName,windowName,pos):
    global setTrackbarByFunction

    setTrackbarByFunction = True
    cv2.setTrackbarPos(trackbarName, windowName, pos)

def removeObjectsWithLowIou(frame,objs):
    global trackedObjects, IOU_THRESHOLD
    for obj in objs:
        if obj.getIOU(str(frame)) < IOU_THRESHOLD:
            obj.removeBox(str(frame))
            obj.addRetrackFrame(str(frame-1), False)
            removeFromList(trackedObjects,obj)

def showHints():
    print("*****Hints*****")
    print("Enter 'a' to add a new annotation.")
    print("Enter 'r' to remove an object (and remaining track).")
    print("Press the SPACE key to resume the program.")
    print("***************")

def waitForUserInput(img,shouldDecrementFrameNumAfterAnnotation=False):
    global frameNum, stopWaitingForUserInput, allObjects, pickleFilePath

    stopWaitingForUserInput = False

    frameNumDidDecrement = False
    userDidAnnotate = False
    
    showHints()

    while True:
        drawImg = img.copy()
        showRectForAllObjects('Webcam',drawImg,frameNum,annotate=True)
        if stopWaitingForUserInput:
            stopWaitingForUserInput = False
            break
        keyPressed2 = cv2.waitKey(1)
        if keyPressed2 == ord('a'):
            userDidAnnotate = True
            userWillAddNewAnnotation(drawImg)
            showHints()
        elif keyPressed2 == ord('c'):
            userWillCorrectBoundingBox(drawImg)
            showHints()
        elif keyPressed2 == ord('r'):
            userWillRemoveObject()
            showHints()
        elif keyPressed2 == ord(' '):
            if userDidAnnotate and shouldDecrementFrameNumAfterAnnotation:
                ### if user annotates, then stay at the current frame to add new objects 
                frameNum -= 1
                frameNumDidDecrement = True
            break
        elif keyPressed2 == ord('e'):
            saveDict(allObjects,pickleFilePath) 
            break

    return frameNumDidDecrement

def deleteAllObjectTracks():
    global allObjects, INITIAL_FRAME

    for o in allObjects:
        o.removeTrackFromFrame(INITIAL_FRAME+1)

def userWillAddNewAnnotation(img):
    global frameNum
    (x,y,w,h) = cv2.selectROI("Webcam", img, False)
    objId = input("Enter object id (enter n for new object): ")
    box = [x,y,x+w,y+h]
    if objId == "n":
        objClass = ""
        if len(allClasses) != 0:
            showAllClasses()
            objClass = input("Enter class number or type a new class: ")
        else:
            objClass = input("Type a new class: ")
        
        # user entered an index for existing object classes
        if len(objClass) == 1:
            classNum = int(objClass)
            if classNum > 0 and classNum < len(allClasses) + 1:
                objClass = getClassNameByNumber(classNum)
        
        addNewClass(objClass)
        modifyBox(objId,frameNum,box,objClass)
    else:
        modifyBox(objId,frameNum,box,retrack=True)

def userWillCorrectBoundingBox(img):
    global frameNum
    (x,y,w,h) = cv2.selectROI("Webcam", img, False)
    objId = input("Enter object id: ")
    box = [x,y,x+w,y+h]
    modifyBox(objId,frameNum,box,retrack=True)

def userWillRemoveObject():
    global frameNum, trackedObjects
    objId = input("Enter object id to be removed: ")
    obj = getObjectWithId(allObjects,objId)
    if obj is not None:
        obj.removeTrackFromFrame(frameNum)
        removeFromList(trackedObjects,obj)

def addNewClass(className):
    global allClasses
    if className not in allClasses:
        allClasses.append(className)

def showAllClasses():
    global allClasses
    for i,c in enumerate(allClasses):
        print(i+1,": ",c)

def getClassNameByNumber(number):
    global allClasses
    return allClasses[number - 1]

def getBboxesFromTracker(img, newObjectsDict, trackedObjectsId):
    global tracker
    
    numObjs = len(trackedObjectsId)

    if newObjectsDict: # if there are new objects to be tracked
        if numObjs == 1: 
            return [tracker.track(trackedObjectsId[0], img[:,:,::-1], newObjectsDict[trackedObjectsId[0]])]
        elif numObjs > 1:
            return tracker.multi_track(trackedObjectsId, img[:,:,::-1], newObjectsDict)
    else:
        if numObjs == 1:
            return [tracker.track(trackedObjectsId[0], img[:,:,::-1])]
        elif numObjs > 1:
            return tracker.multi_track(trackedObjectsId, img[:,:,::-1])
    
    return []

def addToList(targetList,target):
    if target not in targetList:
        targetList.append(target)

def removeFromList(targetList,target):
    if target in targetList:
        targetList.remove(target)

def getObjectWithId(targetList,objectId):
    for target in targetList:
        if objectId == target.getId():
            return target
    return None

def modifyBox(objId,frame,box,objectClass=None,retrack=None):
    global allObjects

    key = str(frame)
    obj = None
    imagePath = getFileNameFromFrame(imageSource,frame,numberOfDigits)

    if objId == "n": # new object
        obj = addNewObject(key,box)
    elif objectWithIdDoesExist(objId,allObjects) is not None:
        obj = objectWithIdDoesExist(objId,allObjects)
        obj.addBox(key,box)
        if retrack:
            obj.addRetrackFrame(key,retrack)
    
    if obj is not None:
        obj.setImagePath(key,imagePath)

    if objectClass is not None:
        obj.setObjectClass(objectClass)

def addNewObject(key,box):
    global idCounter, allObjects
    idCounter += 1
    newObj = GeneralObject.GeneralObject(str(idCounter))
    newObj.addBox(key,box)
    allObjects.append(newObj)
    return newObj

def saveDict(dictToSave,path):
    print("Saving dict......")
    with open(path, 'wb') as handle:
        pickle.dump(dictToSave, handle, protocol=pickle.HIGHEST_PROTOCOL)

def objectWithIdDoesExist(oid,objList):
    for o in objList:
        if o.getId() == oid:
            return o
    return None

def showRectForAllObjects(windowName,image,frame,annotate=None):
    global allObjects

    key = str(frame)

    for obj in allObjects:
        box = obj.getBox(key)
        if box is not None:
            cv2.rectangle(image, 
            (int(box[0]), int(box[1])), 
            (int(box[2]), int(box[3])),
            [0,0,255], PADDING)
            cv2.putText(image, obj.getId(), (int(box[0]), int(box[1]-5)), cv2.FONT_HERSHEY_SIMPLEX, 0.5,(0,0,255),2)
    if annotate is not None:
        if annotate:
            cv2.putText(image, "Please annotate frame: " + key, (5,20), cv2.FONT_HERSHEY_SIMPLEX, 0.5,(0,0,255),2)
        else:
            cv2.putText(image, "Frame: " + key, (5,20), cv2.FONT_HERSHEY_SIMPLEX, 0.5,(0,0,255),2)
    cv2.imshow(windowName, image)

def showAssistanceWindow(windowName,frame):
    global INITIAL, INITIAL_FRAME, ANNOTATION_MODE

    comparisonFrame = frameNum
    
    if ANNOTATION_MODE == MANUAL_MODE:
        comparisonFrame -= MANUAL_MODE_INTERVAL
    elif ANNOTATION_MODE == IOU_STOP_MODE:
        comparisonFrame -= IOU_MODE_INTERVAL
    else:
        comparisonFrame -= 10

    if INITIAL:
        comparisonFrame = INITIAL_FRAME

    if comparisonFrame < 1:
        comparisonFrame = 1

    imgFile = getFileNameFromFrame(imageSource,comparisonFrame,numberOfDigits)
    img = cv2.imread(imgFile)
    showRectForAllObjects(windowName,img,comparisonFrame,False)

def calculateIOU(frame,objects,backward=False):
    global trackerIoUNet, sessIOU
    ## calculate the predicted iou from the IOU net
    targetFrame = frame - COMPARE_FRAME_OFFSET 
    if backward:
        targetFrame = frame + COMPARE_FRAME_OFFSET

    targetKey = str(targetFrame)

    searchFrame = frame
    searchKey = str(searchFrame)
    currentBatch = [[],[],[],[]]
    objectsInBatch = []
    shouldStop = False

    for obj in objects:
        if obj not in trackedObjects and obj.getIOU(searchKey) is not None:
            continue

        if  obj.getBox(targetKey) is not None and obj.getBox(searchKey) is not None:
            # Crop and resize target image
            targetBox = obj.getBox(targetKey)
            targetImg = cv2.imread(getFileNameFromFrame(imageSource,targetFrame,numberOfDigits))
            targetBox = list(map(lambda o: int(o), targetBox))
            (croppedTargetImg, targetCropBox) = im_util.get_cropped_input(targetImg,targetBox,2,227) 
            
            (x1,y1,w,h) = (targetCropBox[0],targetCropBox[1],targetCropBox[2]-targetCropBox[0],targetCropBox[3]-targetCropBox[1])

            # Crop and resize search image
            searchBox = obj.getBox(searchKey)
            searchImg = cv2.imread(getFileNameFromFrame(imageSource,searchFrame,numberOfDigits))
            searchBox = list(map(lambda o: int(o), searchBox))
            (croppedSearchImg, _) = im_util.get_cropped_input(searchImg,searchBox,1,227)

            labelBox = [(searchBox[0]-x1)/w,(searchBox[1]-y1)/h,(searchBox[2]-x1)/w,(searchBox[3]-y1)/h]
            labelBoxTimesTen = [i * 10 for i in labelBox] # scaling the boxes by 10

            ### FOR DEBUGGING: Save the cropped images
            # outputDir = ('./grill/search')
            # cv2.imwrite('%s/%08d.jpg' % (outputDir, frameNum), croppedSearchImg)

            # outputDir = ('./grill/target')
            # cv2.imwrite('%s/%08d.jpg' % (outputDir, frameNum), croppedTargetImg)

            mock_label = [0.123] # the label is not required by the final Tracker-IoU-Net, just a placeholder

            currentBatch[0].append(croppedSearchImg.astype(float))
            currentBatch[1].append(croppedTargetImg.astype(float))
            currentBatch[2].append(labelBoxTimesTen)
            currentBatch[3].append(mock_label)
            objectsInBatch.append(obj)
    
    ious = []
    if currentBatch[0]: # if currentBatch is not empty
        [batch_loss, ious] = sessIOU.run([trackerIoUNet.loss, trackerIoUNet.fc8],feed_dict={trackerIoUNet.image:currentBatch[0],
                trackerIoUNet.target:currentBatch[1], trackerIoUNet.bbox:currentBatch[2], trackerIoUNet.label:currentBatch[3],trackerIoUNet.train_placeholder:False})
        
        ious = list(map(lambda x: 0.1*x, ious))

        for o, iouValue in zip(objectsInBatch,ious):
            o.setIOU(searchKey,iouValue)
    
        # objectsInBatch is a list of objects for which iou prediction is available
        # ious is a list of iou predictions, corresponding to the objects in objectsInBatch

    return (objectsInBatch, ious)

def onTrackbarRough(pos):
    global frameNum, stopFrameTrackbar, trackbarPos, stopWaitingForUserInput, setTrackbarByFunction, setByFineTrackbar, setByRoughTrackbar

    if pos is 0:
        return

    if setTrackbarByFunction:
        setTrackbarByFunction = False
    else:
        stopFrameTrackbar = True
        stopWaitingForUserInput = True

        if setByFineTrackbar:
            setByFineTrackbar = False
        else:
            setByRoughTrackbar = True
            cv2.setTrackbarPos("test2","Webcam",50)
        
    trackbarPos = pos

    return

def onTrackbarFine(pos):
    global frameNum, prevFineTrackbarPos, setByFineTrackbar, setByRoughTrackbar, stopFrameTrackbar, stopWaitingForUserInput

    increase = pos - prevFineTrackbarPos
    prevFineTrackbarPos = pos

    if setByRoughTrackbar:
        setByRoughTrackbar = False
    else:
        setByFineTrackbar = True
        cv2.setTrackbarPos("test","Webcam",frameNum + increase)

    return

# printIOUs(baseFrame,obj,bbox):
# This function was only used to create images for the documentation
def printIOUs(baseFrame,obj,bbox):
    
    currentBatch = [ [], [], [], [] ]

    # Crop and resize search image
    imgFile = getFileNameFromFrame(imageSource,baseFrame,numberOfDigits)
    img = cv2.imread(imgFile)
    search_box = [int(bbox[0]),int(bbox[1]),int(bbox[2]),int(bbox[3])]
    (cropped_search_img, _) = im_util.get_cropped_input(img,search_box,1,227)

    if not os.path.exists('./which-frames'):
        os.mkdir('./which-frames')

    if not os.path.exists('./%d' % (baseFrame)):
        os.mkdir('./which-frames/%d' % (baseFrame))

    if not os.path.exists('./%d/target' % (baseFrame)):
        os.mkdir('./which-frames/%d/target' % (baseFrame))

    if not os.path.exists('./%d/search' % (baseFrame)):
        os.mkdir('./which-frames/%d/search' % (baseFrame))

    ious = open("./which-frames/labels_iou_frame-%d.txt" % (baseFrame), "w")
    ious.close()

    for i in range(10):
        frame = str(baseFrame - i - 1)
        frameBox = obj.getBox(frame)
        target_img = cv2.imread(getFileNameFromFrame(imageSource,frame,numberOfDigits))
        target_box = [int(frameBox[0]),int(frameBox[1]),int(frameBox[2]),int(frameBox[3])]
        (cropped_target_img, target_crop_box) = im_util.get_cropped_input(target_img,target_box,2,227) 
        
        (x1,y1,w,h) = (target_crop_box[0],target_crop_box[1],target_crop_box[2]-target_crop_box[0],target_crop_box[3]-target_crop_box[1])

        label_box = [(search_box[0]-x1)/w,(search_box[1]-y1)/h,(search_box[2]-x1)/w,(search_box[3]-y1)/h]
        label_box_10 = [i * 10 for i in label_box] # scaling the boxes by 10

        mock_label = [0.123]

        ### FOR DEBUGGING: Save the cropped images
        if i == 0:
            outputDir = ('./which-frames/%d/search' % (baseFrame))
            cv2.imwrite('%s/%s.jpg' % (outputDir, frame), cropped_search_img)

        outputDir = ('./which-frames/%d/target' % (baseFrame))
        cv2.imwrite('%s/%s.jpg' % (outputDir, frame), cropped_target_img)

        currentBatch[0].append(cropped_search_img.astype(float))
        currentBatch[1].append(cropped_target_img.astype(float))
        currentBatch[2].append(label_box_10)
        currentBatch[3].append([0.123])
        
    [batch_loss, fc8] = sessIOU.run([trackerIoUNet.loss, trackerIoUNet.fc8],feed_dict={trackerIoUNet.image:currentBatch[0],
        trackerIoUNet.target:currentBatch[1], trackerIoUNet.bbox:currentBatch[2], trackerIoUNet.label:currentBatch[3],trackerIoUNet.train_placeholder:False})

    ious = open("./which-frames/labels_iou_frame-%d.txt" % (baseFrame), "a")
    ious.write(frame + ": " + str(fc8))
    ious.close()

def setComparisonFrame(fps):
    global COMPARE_FRAME_OFFSET
    if fps > 30:
        COMPARE_FRAME_OFFSET = 20
    else:
        COMPARE_FRAME_OFFSET = 10

# Main function
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Semi-automatic video annotation.')
    parser.add_argument('-m', '--annotation_mode', type=str, default="iou")
    parser.add_argument('-t', '--timer', action='store_true', default=False)
    parser.add_argument('-i', '--interval', type=int, default=100)
    parser.add_argument('-v', '--video', type=str)
    args = parser.parse_args()
    
    TIMER = args.timer # timer mode
    IOU_MODE_INTERVAL = args.interval

    videoFileName = args.video # video name including extension
    if videoFileName is None:
        videoFileName = input("Please enter video name (including extension):")

    videoName = videoFileName.split(".")[0]
    imageSource = "./outputs/%s/" % (videoName)

    # set annotation modes
    if args.annotation_mode == "manual" or args.annotation_mode == "1":
        ANNOTATION_MODE = 1
    elif args.annotation_mode == "tracker" or args.annotation_mode == "2":
        ANNOTATION_MODE = 2
    elif args.annotation_mode == "iou-stop" or args.annotation_mode == "4":
        ANNOTATION_MODE = 4
    else:
        ANNOTATION_MODE = 3

    # Use a large number for IOU_MODE_INTERVAL in IOU_MODE -> program should not pause automatically
    if ANNOTATION_MODE == IOU_MODE:
        IOU_MODE_INTERVAL = 2**30

    tracker = re3_tracker.Re3Tracker()

    ckpt_dir = "../TrackerIoUNet/checkpoint"
    if not os.path.exists(ckpt_dir):
        os.makedirs(ckpt_dir)
    ckpt = tf.train.get_checkpoint_state(ckpt_dir)

    # Build a separate graph containing `iou_net`
    with tf.Graph().as_default() as iou_net_graph:
        trackerIoUNet = TrackerIoUNet.TrackerIoUNet(1,train=False)
        trackerIoUNet.build()
        saver2 = tf.train.Saver()
        sessIOU = tf.Session(graph=iou_net_graph)
        init = tf.global_variables_initializer()
        init_local = tf.local_variables_initializer()
        sessIOU.run(init)
        sessIOU.run(init_local)
        
        coord = tf.train.Coordinator()
        # start the threads
        tf.train.start_queue_runners(sess=sessIOU, coord=coord)

        saver2.restore(sessIOU, ckpt.model_checkpoint_path)
    ## END IOU_NET  

    # Retore allObjects
    if not os.path.exists('./pickles'):
        os.mkdir('./pickles')
    pickleFileName = '%s.pickle' % (videoName)
    pickleFilePath = './pickles/%s' % (pickleFileName)
    if not TIMER and os.path.exists(pickleFilePath):
        with open(pickleFilePath, 'rb') as handle:
            allObjects = pickle.load(handle)

    # restore idCounter and list of object class
    for o in allObjects:
        if int(o.getId()) > idCounter:
            idCounter = int(o.getId())
        
        addToList(allClasses,o.getObjectClass())

    # Count total number of frames    
    totalNumberOfFrames = len(glob.glob1(imageSource,"*.jpg"))
    print("Total number of frames:", totalNumberOfFrames)

    # detect the frame rate 
    video = cv2.VideoCapture('./videos/%s' % (videoFileName))
    fps = video.get(cv2.CAP_PROP_FPS)
    print("Frame rate: ",fps)

    # set comparison frame according to the frame rate
    setComparisonFrame(fps)

    cv2.namedWindow("Webcam")
    cv2.createTrackbar("test", "Webcam", 1, totalNumberOfFrames, onTrackbarRough)
    cv2.createTrackbar("test2", "Webcam", 50, 100, onTrackbarFine)

    startAnnotation()


