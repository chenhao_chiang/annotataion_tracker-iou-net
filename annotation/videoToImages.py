'''
Author: Chen-Hao Chiang
Created in December 2018
'''
import cv2
import argparse
import numpy as np
import os

def createImagesFromVideo(videoFileName):
    videoName = videoFileName.split(".")[0]
    video = cv2.VideoCapture('./videos/%s' % (videoFileName))
    frameNum = 1
    outputDir = None

    fps = video.get(cv2.CAP_PROP_FPS)
    print("frame rate: ", fps)

    if not os.path.exists('outputs'):
        os.mkdir('outputs')

    outputDir = ('outputs/%s/' % (videoName))
    if not os.path.exists(outputDir):
        os.mkdir('outputs/%s' % (videoName))

    while True:
        ret_val, img = video.read()

        if img is None:
            break

        origImg = img.copy()

        cv2.imwrite('%s%08d.jpg' % (outputDir, frameNum), origImg)

        print("Extracting frame: ", frameNum)
           
        frameNum += 1

    cv2.destroyAllWindows()



# Main function
if __name__ == '__main__':
    parser = argparse.ArgumentParser(
            description='Show the Webcam demo.')
    parser.add_argument('-v', '--video', type=str)
    args = parser.parse_args()
    videoFileName = args.video
    if videoFileName is None:
        videoFileName = input("Please enter video name (including extension):")

    createImagesFromVideo(videoFileName)


