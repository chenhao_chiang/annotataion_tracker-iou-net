import numpy as np
import glob
import os

def get_data_for_dataset(dataset_name, mode):
    # Implement this for each dataset.
    #gt = {}
    #image_paths = []
    print("in get dataset")
    print(dataset_name)
    if dataset_name == 'alov300+++':
        datadir = os.path.join(
                os.path.dirname(__file__),
                'datasets',
                'alov300+++')
        gt = np.load(datadir + '/labels/' + mode + '/labels.npy')
        image_paths = [datadir + '/' + line.strip()
            for line in open(datadir + '/labels/' + mode + '/image_names.txt')]
    return {
            'gt' : gt,
            'image_paths' : image_paths,
            }

