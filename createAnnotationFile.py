'''
Author: Chen-Hao Chiang
Created in March 2019
'''
import pickle
import os
import glob
import uuid
import argparse
from PIL import Image

from lxml import etree

def write_pascal_voc(filepath, imageFile, boxes):
    def sub_el_w_text(parent, name, text=None):
        el = etree.SubElement(parent, name)
        if text is not None:
            el.text = str(text)
        return el
    # print("Saving {} with boxes {}".format(filepath, boxes))
    root = etree.Element("annotation")
    sub_el_w_text(root, "folder", "labels")
    sub_el_w_text(root, "filename", imageFile.split("/")[-1])
    sub_el_w_text(root, "path", imageFile)

    source = sub_el_w_text(root, "source")
    sub_el_w_text(source, "database", "Unknown")

    im = Image.open(imageFile)
    width, height = im.size

    size = sub_el_w_text(root, "size")
    sub_el_w_text(size, "width", width)
    sub_el_w_text(size, "height", height)

    sub_el_w_text(root, "segmented", 0)

    for box in boxes:
        obj = sub_el_w_text(root, "object")
        sub_el_w_text(obj, "name", box["label"])
        sub_el_w_text(obj, "pose", "Unspecified")
        sub_el_w_text(obj, "truncated", 1)
        sub_el_w_text(obj, "difficult", 0)
        bndbox = sub_el_w_text(obj, "bndbox")
        xmin = max(0, int(box["box"][0]))
        ymin = max(0, int(box["box"][1]))
        xmax = max(0, xmin + int(box["box"][2]))
        ymax = max(0, ymin + int(box["box"][3]))
        sub_el_w_text(bndbox, "xmin", xmin)
        sub_el_w_text(bndbox, "ymin", ymin)
        sub_el_w_text(bndbox, "xmax", xmax)
        sub_el_w_text(bndbox, "ymax", ymax)

    with open(filepath, "wb") as xmlf:
        xmlf.write(etree.tostring(root, pretty_print=True))

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Semi-automatic video annotation.')
    parser.add_argument('-v', '--video', type=str)
    args = parser.parse_args()

    videoFileName = args.video # video name including extension
    if videoFileName is None:
        videoFileName = input("Please enter video name (including extension):")
    
    videoName = videoFileName.split(".")[0]
    pickleFileName = './annotation/pickles/' + videoName + ".pickle"
    imageSource = './annotation/outputs/%s/' % (videoName)
    # Count total number of frames    
    totalNumberOfFrames = len(glob.glob1(imageSource,"*.jpg"))
    print("Total number of frames:", totalNumberOfFrames)

    allObjects = []

    if os.path.exists(pickleFileName):
        with open(pickleFileName, 'rb') as handle:
            allObjects = pickle.load(handle)

    if not os.path.exists('./labels'):
        os.mkdir('./labels')

    if not os.path.exists('./labels/' + videoName):
        os.mkdir('./labels/' + videoName)



    for i in range(totalNumberOfFrames):
        key = str(i+1)
        
        boxes = []        
        fileName = './labels/' + videoName + '/' + key.zfill(8) + ".xml"
        imageFile = imageSource + key.zfill(8) + ".jpg"
            
        for o in allObjects:
            if key in o.getTrack():
                boxInfo = {}
                boxInfo["box"] = o.getBox(key)
                boxInfo["label"] = o.getObjectClass()
                boxes.append(boxInfo)
        
        if len(boxes) > 0:
            write_pascal_voc(fileName, imageFile, boxes)
