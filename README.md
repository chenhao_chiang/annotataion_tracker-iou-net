# Semi-automatic Annotation Framework with the Re3 Tracker and the Tracker-IoU-Net
This semi-automatic annotation tool is based on the framework proposed in the Master's thesis "Speeding up the Generation of Image-Label Pairs from Videos Using Machine Learning" by Chen-Hao Chiang. The thesis was completed at TUM Department of Informatics.

The annotation uses the Re3 tracker by D. Gordon, A. Farhadi, and D. Fox., and the Tracker-IoU-Net proposed in the thesis mentioned above. The original paper of the Re3 tracker can be found on: https://arxiv.org/pdf/1705.06368.pdf.


## Re3 Tracker Model:
1. Download the Re3 model at http://bit.ly/2L5deYF.
2. Place the unzipped checkpoint files under "logs/checkpoints". 

## Tracker-IoU-Net Model:
1. Download the pre-trained model at: https://drive.google.com/open?id=1NGmPCZNU5NXRNxGNi8IEyBgPnz9ZLJry
2. Create a folder "checkpoint" under "TrackerIoUNet" and place the checkpoint files in there.
3. The Tracker-IoU-Net is based on the TensorFlow implementation of the GOTURN Tracker. For more information, please visit the Tracker-IoU-Net repository. https://bitbucket.org/chenhao_chiang/tracker-iou-net/src/master/

## First Time Setup:
Here are the steps for setting up the environment on MacOS.

1. Install python 

        brew install python


2. Clone the repository
    
        git clone https://bitbucket.org/chenhao_chiang/annotataion_tracker-iou-net/
        cd annotataion_Tracker-IoU-Net
    

3. Download pip and create a virtual environment

        sudo easy_install pip
        sudo pip install virtualenv
        virtualenv [your_env] -p python3.6
    

4. Activate the virtual environment and install the requirements

        source [your_env]/bin/activate
        pip install -r requirements.txt
   

5. Leave the virtual environment when you're finished

        deactivate
  

## Requirements:
1. Python 3.6+.
2. [Tensorflow](https://www.tensorflow.org/) and its requirements. 
3. [NumPy](http://www.numpy.org/). 
4. [OpenCV 2](http://opencv.org/opencv-2-4-8.html). 
5. [CUDA (Strongly Recommended)](https://developer.nvidia.com/cuda-downloads).
6. [cuDNN (Recommended)](https://developer.nvidia.com/cudnn).


## How to use the anntation tool:
### Extract images from a video:
1. Place the video under "annotation/videos".
2. Under "annotation", run "videoToImages.py". 
3. A folder with the same name as the video will be created under "annotation/outputs", in which the extracted images are saved.

### Start annotating:
1. Under "annotation", run "startAnnotation.py"
2. Press "e" or CTRL+C to exit, the annotations will be saved automatically.
    WARNING: If you terminate the program with CTRL+Z, the annotations will be lost!
3. A pickle file with the same name as the video will be created under "annotation/pickles".

### Output annotations:
1. Run "createAnnotationFile.py" under "annotation"
2. View the annotation files under "labels/[name_of_video]"

## License and Citation
1. The semi-automatic annotation framework is released under the GPL V3.
2. The Tracker-IoU-Net is released under the MIT License.