import numpy as np

class GeneralObject:
    def __init__(self, objectId):
        self.id = objectId
        self.track = dict() # {frame(String): box(List)}
        self.retrackFrames = dict() # {frame(String): re-track(Bool)}
        self.imagePathDict = dict() # {frame(String): imagePath(String)}
        self.iouDict = dict() # {frame(String): iou(Float)}
        self.objectClass = "Unknown"
        self.trackedBackward = False
        

    def getId(self):
        return self.id

    def getBox(self,key):
        if key in self.track:
            return self.track[key]
        else:
            return None

    def getTrack(self):
        return self.track

    def addBox(self,key,box):
        self.track[key] = box

    def setObjectClass(self,className):
        self.objectClass = className

    def getObjectClass(self):
        return self.objectClass

    def setImagePath(self,key,imagePath):
        self.imagePathDict[key] = imagePath

    def getImagePath(self,key):
        if key in self.imagePathDict:    
            return self.imagePathDict[key]
        else:
            return None

    def setIOU(self,key,iou):
        self.iouDict[key] = iou

    def deleteIOU(self,key):
        if self.getIOU(key) is not None:
            del self.iouDict[key]

    def getIOU(self,key):
        if key in self.iouDict:
            return self.iouDict[key]
        else:
            return None

    def setTrackedBackward(self,value):
        self.trackedBackward = value

    def getTrackedBackward(self):
        return self.trackedBackward

    def removeBox(self,key):
        if key in self.track:
            del self.track[key]

    def addRetrackFrame(self,key,value):
        self.retrackFrames[key] = value

    def retrackForFrame(self,key):
        if key in self.retrackFrames:
            return self.retrackFrames[key]
        return None
    
    def removeTrackFromFrame(self,frameNum):
        f = frameNum
        self.addRetrackFrame(str(f-1),False)
        while self.getBox(str(f)) is not None:
            # print("delete frame: ",f)
            self.removeBox(str(f))

            if self.retrackForFrame(str(f)) is not None:
                self.addRetrackFrame(str(f),None)
            
            if self.getIOU(str(f)) is not None:
                self.deleteIOU(str(f))

            f = f + 1
            


